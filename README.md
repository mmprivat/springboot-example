# Springboot Example

## Source Code
Can be found at https://bitbucket.org/mmprivat/springboot-example

## Description

This project represents the very first steps with springboot.

I might extend it in the future with spring security (authentication & authorisation), how to do microservices,...

## Prerequisites
* Java 1.8
* Maven 3

## Compile

call 
`mvn clean install`
to compile it.

## Run

run
`be.manhart.springboot.Application`
as java application in your IDE, and call
`http://127.0.0.1:8080/register` in your browser.
Optionally you can add a param `?name=YourUserName`.

## Development

-

## License

This software project is licensed under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
