package be.manhart.springboot;

/**
 * We define all our constants in here
 * 
 * @author manuel
 *
 */
public class Constants {

	/**
	 * this is for defining the result of our REST services - since the default is
	 * application/json it is actually unneccessary.
	 */
	public static final String APPLICATION_JSON_VALUE = "application/json";

}
