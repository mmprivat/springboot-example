package be.manhart.springboot.rest.registration;

public class UserData {
	private long id;
	private String name;
	
	public UserData(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public UserData() {
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
