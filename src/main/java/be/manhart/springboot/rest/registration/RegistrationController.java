package be.manhart.springboot.rest.registration;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import be.manhart.springboot.Constants;

/**
 * This is our first REST controller. It handles the requests we define him to
 * handle.
 * 
 * @author manuel
 *
 */
@RestController
public class RegistrationController {
	/**
	 * This is a template for returning a greeting of the user on the register call
	 */
	private static final String template = "Hello, %s!";
	/**
	 * This is just for counting the id upwards.
	 */
	private final AtomicLong counter = new AtomicLong();

	// for future use
	// @RequestMapping(method = RequestMethod.POST, value = "/register", produces =
	// Constants.APPLICATION_JSON_VALUE)
	// public UserData register(@RequestBody Registration user) {
	//
	// boolean usernameAlreadyExists = false;
	// if (usernameAlreadyExists) {
	// throw new IllegalArgumentException("error.username");
	// }
	// return new UserData();
	// }

	/**
	 * This method handles the REST call and returns a greeting. Optionally you can
	 * add a parameter "name" for the name of the user.<br/>
	 * e.g. http://127.0.0.1:8080/register?name=Manuel
	 * 
	 * @param name
	 *            optional param for the name
	 * @return a {@link UserData} object which will be converted to json (you could
	 *         also use xml) by spring boot automatically
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/register", produces = Constants.APPLICATION_JSON_VALUE)
	public UserData register(@RequestParam(value = "name", defaultValue = "DefaultUser") String name) {
		return new UserData(counter.incrementAndGet(), String.format(template, name));
	}

	// for future use
	// @ExceptionHandler
	// void handleIllegalArgumentException(IllegalArgumentException e,
	// HttpServletResponse response) throws IOException {
	// response.sendError(HttpStatus.BAD_REQUEST.value());
	// }
}