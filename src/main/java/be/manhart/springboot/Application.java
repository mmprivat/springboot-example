package be.manhart.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the starting point of our spring boot application. To try it, run
 * this as Java Application and browse http://127.0.0.1:8080/register
 * 
 * @author manuel
 *
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
